/* 
 * JS загрузки погоды
 */
$(function(){    
        
        $weather = $('#weather');
        
        itemLoad();
});

function itemLoad(){
         
        $.ajax({
                    url: '/api/cities/350000/weather',
                    type: 'GET',                                 
                    dataType: "json",                                           
                    success: function(response){
                            
                            var prefix = "+",
                                temperature = parseInt(response.temperature);    
                            
                            $weather.text((temperature > 0 ? prefix + temperature : temperature));                            
                    }       
        });
}                                               
