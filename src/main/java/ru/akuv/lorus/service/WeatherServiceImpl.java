package ru.akuv.lorus.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.akuv.lorus.dao.IWeatherDao;
import ru.akuv.lorus.domain.Weather;

/**
 * Service-слой управления погодой
 * @author Alex
 */
@Service("weatherService")
public class WeatherServiceImpl implements IWeatherService {
    
    @Autowired
    private IWeatherDao weatherDao;
        
    @Override
    public Weather getItem(String postCode) {
                        
        return weatherDao.getItem(postCode);  
    }
    
}
