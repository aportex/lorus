package ru.akuv.lorus.service;

import ru.akuv.lorus.domain.Weather;

/**
 * Service-слой управления погодой
 * @author Alex
 */
public interface IWeatherService {
    
    public Weather getItem(String postCode);                                    //получить
            
}
