package ru.akuv.lorus.domain;
        
import java.io.Serializable;
import java.util.Objects;

/**
 * Домен погоды
 * @author Alex
 */

public class Weather implements Serializable {
    
    private Float temperature;    

    public Float getTemperature() {
        return temperature;
    }

    public void setTemperature(Float temperature) {
        this.temperature = temperature;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.temperature);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Weather other = (Weather) obj;
        if (!Objects.equals(this.temperature, other.temperature)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Weather{" + "temperature=" + temperature + '}';
    }                          
}
