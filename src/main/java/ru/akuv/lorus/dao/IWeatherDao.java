package ru.akuv.lorus.dao;

import ru.akuv.lorus.domain.Weather;

/**
 * DAO-слой управления погодой
 * @author Alex
 */
public interface IWeatherDao {
         
    public Weather getItem(String postCode);                                    //получить
    
}
