package ru.akuv.lorus.dao;


import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import ru.akuv.lorus.domain.Weather;


/**
 * DAO-слой управления погодой
 * @author Alex
 */
@Repository
public class WeatherDaoImpl implements IWeatherDao{
                
    @Override
    public Weather getItem(String postCode) {
                
        StringBuilder URL = new StringBuilder("http://dataservice.accuweather.com/forecasts/v1/hourly/1hour/")
                .append(postCode)
                .append("?apikey=U2MurTTjV7F5pU1GyMX36AaF3afELD08&language=ru-ru");
        
        ResponseEntity<String> response = new RestTemplate().getForEntity(URL.toString(), String.class);
        
        Weather w = null;        
        JSONParser parser = new JSONParser(); 
        JSONArray jsonArr = null;
        JSONObject jsonObj = null;
        
        try {
            
            jsonArr = (JSONArray) parser.parse(response.getBody());
            jsonObj = (JSONObject) jsonArr.get(0);
            jsonObj = (JSONObject) jsonObj.get("Temperature");   
            
        } catch (ParseException ex) {
            
            Logger.getLogger(WeatherDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(jsonObj.get("Value") != null) {
        
            w = new Weather();        
            w.setTemperature(Float.valueOf(jsonObj.get("Value").toString()));        
        }
                
        return w;
    }  
                
}
