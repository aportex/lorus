package ru.akuv.lorus.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;
import ru.akuv.lorus.domain.Weather;
import ru.akuv.lorus.service.IWeatherService;

/**
 * Контроллер управления погодой
 * @author Alex
 */
@Controller
@RequestMapping("api")
public class WeatherController extends AbstractController {
        
    @Autowired
    private IWeatherService weatherService;  
            
    public WeatherController() {
    }
    
    protected ModelAndView handleRequestInternal(
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        throw new UnsupportedOperationException("Not yet implemented");
    }
                        
    /** 
     * Получение погоды    
     */
    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value={"/cities/{postCode}/weather"}, method= RequestMethod.GET)    
    public @ResponseBody Weather getItem(@PathVariable("postCode") String postCode)
    {      
        
        return weatherService.getItem(postCode);  
        
    }     
}
