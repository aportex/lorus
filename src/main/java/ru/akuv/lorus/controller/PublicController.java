package ru.akuv.lorus.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

/*
 * Контроллер-диспетчер
 * @author Alex
 */
@Controller
@RequestMapping("/")
public class PublicController extends AbstractController {
                        
    public PublicController() {
    }
            
    protected ModelAndView handleRequestInternal(
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        throw new UnsupportedOperationException("Not yet implemented");
    }
                                                                                
    /**                                                                         
     * Переход на главную страницу                                        
     */
    @RequestMapping(value={"/"}, method= RequestMethod.GET)
    protected String toMain(Model model)
    {         
        model.addAttribute("action", "main");        
        model.addAttribute("weather", "+20");                          
           
        return "public";       
    }       
    
    /** 
     * Переход на страницу авторизации
     */
    @RequestMapping(value={"/login"}, method= RequestMethod.GET)
    protected String toLogin(Model model)
    {
        model.addAttribute("action", "login");    
          
        return "public";          
    }
    
    /** 
     * Переход на страницу авторизации после неудачной авторизации
     */
    @RequestMapping(value={"/login/failure"}, method= RequestMethod.GET)
    protected String toLoginFail(Model model)
    {
                            
        model.addAttribute("action", "login"); 
        model.addAttribute("login_error", "Неудачная авторизация");
        
        return "public";        
    }
}
